﻿namespace EDUSIMS.Service.BLL
{
    using EDUSIMS.Core.Model;
    using EDUSIMS.Infrastructure.UnitOfWork;
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public class ApplicantDirectEntryResultService : IApplicantDirectEntryResultService
    {
        private UnitOfWork uow = new UnitOfWork();

        public void AddApplicantDirectEntryResult(ApplicantDirectEntryResult ApplicantDirectEntryResult)
        {
            this.uow.RepositoryFor<ApplicantDirectEntryResult>().Add(ApplicantDirectEntryResult);
            this.uow.Save();
        }

        public void AddOrEditApplicantDirectEntryResult(ApplicantDirectEntryResult ApplicantDirectEntryResult)
        {
            if (ApplicantDirectEntryResult.ID == Guid.Empty)
            {
                this.AddApplicantDirectEntryResult(ApplicantDirectEntryResult);
            }
            else
            {
                this.EditApplicantDirectEntryResult(ApplicantDirectEntryResult);
            }
        }

        public void EditApplicantDirectEntryResult(ApplicantDirectEntryResult ApplicantDirectEntryResult)
        {
            this.uow.RepositoryFor<ApplicantDirectEntryResult>().Edit(ApplicantDirectEntryResult);
            this.uow.Save();
        }

        public IEnumerable<ApplicantDirectEntryResult> GetAllApplicantDirectEntryResult() => 
            (from a in this.GetAllApplicantDirectEntryResultIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<ApplicantDirectEntryResult> GetAllApplicantDirectEntryResultIncludingDeleted() => 
            this.uow.RepositoryFor<ApplicantDirectEntryResult>().GetAll();

        public ApplicantDirectEntryResult GetApplicantDirectEntryResult(Guid id) => 
            this.uow.RepositoryFor<ApplicantDirectEntryResult>().Get(id);

        public ApplicantDirectEntryResult GetByKey(Guid key) => 
            (from a in this.GetAllApplicantDirectEntryResult()
                where a.Key == key
                select a).FirstOrDefault<ApplicantDirectEntryResult>();

        public void RemoveApplicantDirectEntryResult(Guid id)
        {
            ApplicantDirectEntryResult applicantDirectEntryResult = this.GetApplicantDirectEntryResult(id);
            applicantDirectEntryResult.IsDelete = true;
            this.EditApplicantDirectEntryResult(applicantDirectEntryResult);
        }

        public double TotalApplicantDirectEntryResult() => 
            ((double) this.GetAllApplicantDirectEntryResult().Count<ApplicantDirectEntryResult>());

      
    }
}

