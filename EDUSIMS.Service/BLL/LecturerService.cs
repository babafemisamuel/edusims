﻿namespace EDUSIMS.Service.BLL
{
    using EDUSIMS.Core.Model;
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public class LecturerService : ILecturerService
    {
        private EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork uow = new EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork();

        public void AddLecturer(Lecturer Lecturer)
        {
            this.uow.RepositoryFor<Lecturer>().Add(Lecturer);
            this.uow.Save();
        }

        public void AddOrEditLecturer(Lecturer Lecturer)
        {
            if (Lecturer.ID == Guid.Empty)
            {
                this.AddLecturer(Lecturer);
            }
            else
            {
                this.EditLecturer(Lecturer);
            }
        }

        public void EditLecturer(Lecturer Lecturer)
        {
            this.uow.RepositoryFor<Lecturer>().Edit(Lecturer);
            this.uow.Save();
        }

        public IEnumerable<Lecturer> GetAllLecturer() => 
            (from a in this.GetAllLecturerIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<Lecturer> GetAllLecturerIncludingDeleted() => 
            this.uow.RepositoryFor<Lecturer>().GetAll();

        public Lecturer GetByKey(Guid key) => 
            (from a in this.GetAllLecturer()
                where a.Key == key
                select a).FirstOrDefault<Lecturer>();

        public Lecturer GetLecturer(Guid id) => 
            this.uow.RepositoryFor<Lecturer>().Get(id);

        public void RemoveLecturer(Guid id)
        {
            Lecturer lecturer = this.GetLecturer(id);
            lecturer.IsDelete = true;
            this.EditLecturer(lecturer);
        }

        public double TotalLecturer() => 
            ((double) this.GetAllLecturer().Count<Lecturer>());

       
    }
}

