﻿namespace EDUSIMS.Service.BLL
{
    
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using EDUSIMS.Core.Model;

    public class ApplicantDegreeQualificationsService : IApplicantDegreeQualificationsService
    {
        private EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork uow = new EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork();

        public void AddApplicantDegreeQualification(ApplicantDegreeQualification ApplicantDegreeQualification)
        {
            this.uow.RepositoryFor<ApplicantDegreeQualification>().Add(ApplicantDegreeQualification);
            this.uow.Save();
        }

        public void AddOrEditApplicantDegreeQualification(ApplicantDegreeQualification ApplicantDegreeQualification)
        {
            if (ApplicantDegreeQualification.ID == Guid.Empty)
            {
                this.AddApplicantDegreeQualification(ApplicantDegreeQualification);
            }
            else
            {
                this.EditApplicantDegreeQualification(ApplicantDegreeQualification);
            }
        }

        public void EditApplicantDegreeQualification(ApplicantDegreeQualification ApplicantDegreeQualification)
        {
            this.uow.RepositoryFor<ApplicantDegreeQualification>().Edit(ApplicantDegreeQualification);
            this.uow.Save();
        }

        public IEnumerable<ApplicantDegreeQualification> GetAllApplicantDegreeQualification() => 
            (from a in this.GetAllApplicantDegreeQualificationIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<ApplicantDegreeQualification> GetAllApplicantDegreeQualificationIncludingDeleted() => 
            this.uow.RepositoryFor<ApplicantDegreeQualification>().GetAll();

        public ApplicantDegreeQualification GetApplicantDegreeQualification(Guid id) => 
            this.uow.RepositoryFor<ApplicantDegreeQualification>().Get(id);

        public ApplicantDegreeQualification GetByKey(Guid key) => 
            (from a in this.GetAllApplicantDegreeQualification()
                where a.Key == key
                select a).FirstOrDefault<ApplicantDegreeQualification>();

        public void RemoveApplicantDegreeQualification(Guid id)
        {
            ApplicantDegreeQualification applicantDegreeQualification = this.GetApplicantDegreeQualification(id);
            applicantDegreeQualification.IsDelete = true;
            this.EditApplicantDegreeQualification(applicantDegreeQualification);
        }

        public double TotalApplicantDegreeQualification() => 
            ((double) this.GetAllApplicantDegreeQualification().Count<ApplicantDegreeQualification>());

      
    }
}

