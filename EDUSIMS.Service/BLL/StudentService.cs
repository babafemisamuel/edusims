﻿namespace EDUSIMS.Service.BLL
{
    using EDUSIMS.Infrastructure.UnitOfWork;
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using EDUSIMS.Core.Model;

    public class StudentService : IStudentService
    {
        private EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork uow = new EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork();

        public void AddOrEditStudent(Student Student)
        {
            if (Student.ID == Guid.Empty)
            {
                this.AddStudent(Student);
            }
            else
            {
                this.EditStudent(Student);
            }
        }

        public void AddStudent(Student Student)
        {
            this.uow.RepositoryFor<Student>().Add(Student);
            this.uow.Save();
        }

        public void EditStudent(Student Student)
        {
            this.uow.RepositoryFor<Student>().Edit(Student);
            this.uow.Save();
        }

        public IEnumerable<Student> GetAllStudent() => 
            (from a in this.GetAllStudentIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<Student> GetAllStudentIncludingDeleted() => 
            this.uow.RepositoryFor<Student>().GetAll();

        public Student GetByKey(Guid key) => 
            (from a in this.GetAllStudent()
                where a.Key == key
                select a).FirstOrDefault<Student>();

        public Student GetStudent(Guid id) => 
            this.uow.RepositoryFor<Student>().Get(id);

        public void RemoveStudent(Guid id)
        {
            Student student = this.GetStudent(id);
            student.IsDelete = true;
            this.EditStudent(student);
        }

        public double TotalStudent() => 
            ((double) this.GetAllStudent().Count<Student>());

        
    }
}

