﻿namespace EDUSIMS.Service.BLL
{
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using EDUSIMS.Core.Model;

    public class ApplicantJambResultService : IApplicantJambResultService
    {
        private EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork uow = new EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork();

        public void AddApplicantJambResult(ApplicantJambResult ApplicantJambResult)
        {
            this.uow.RepositoryFor<ApplicantJambResult>().Add(ApplicantJambResult);
            this.uow.Save();
        }

        public void AddOrEditApplicantJambResult(ApplicantJambResult ApplicantJambResult)
        {
            if (ApplicantJambResult.ID == Guid.Empty)
            {
                this.AddApplicantJambResult(ApplicantJambResult);
            }
            else
            {
                this.EditApplicantJambResult(ApplicantJambResult);
            }
        }

        public void EditApplicantJambResult(ApplicantJambResult ApplicantJambResult)
        {
            this.uow.RepositoryFor<ApplicantJambResult>().Edit(ApplicantJambResult);
            this.uow.Save();
        }

        public IEnumerable<ApplicantJambResult> GetAllApplicantJambResult() => 
            (from a in this.GetAllApplicantJambResultIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<ApplicantJambResult> GetAllApplicantJambResultIncludingDeleted() => 
            this.uow.RepositoryFor<ApplicantJambResult>().GetAll();

        public ApplicantJambResult GetApplicantJambResult(Guid id) => 
            this.uow.RepositoryFor<ApplicantJambResult>().Get(id);

        public ApplicantJambResult GetByKey(Guid key) => 
            (from a in this.GetAllApplicantJambResult()
                where a.Key == key
                select a).FirstOrDefault<ApplicantJambResult>();

        public void RemoveApplicantJambResult(Guid id)
        {
            ApplicantJambResult applicantJambResult = this.GetApplicantJambResult(id);
            applicantJambResult.IsDelete = true;
            this.EditApplicantJambResult(applicantJambResult);
        }

        public double TotalApplicantJambResult() => 
            ((double) this.GetAllApplicantJambResult().Count<ApplicantJambResult>());

       
    }
}

