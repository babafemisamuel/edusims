﻿namespace EDUSIMS.Service.BLL
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface IAcademicSessionService
    {
        void AddAcademicSession(AcademicSession AcademicSession);
        void AddOrEditAcademicSession(AcademicSession AcademicSession);
        void EditAcademicSession(AcademicSession AcademicSession);
        AcademicSession GetAcademicSession(Guid id);
        IEnumerable<AcademicSession> GetAllAcademicSession();
        IEnumerable<AcademicSession> GetAllAcademicSessionIncludingDeleted();
        AcademicSession GetByKey(Guid key);
        void RemoveAcademicSession(Guid id);
        double TotalAcademicSession();
    }
}

