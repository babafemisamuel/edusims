﻿namespace EDUSIMS.Service.BLL
{
    using EDUSIMS.Core.Model;
    using EDUSIMS.Infrastructure.UnitOfWork;
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public class CourseLecturerService : ICourseLecturerService
    {
        private EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork uow = new EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork();

        public void AddCourseLecturer(CourseLecturer CourseLecturer)
        {
            this.uow.RepositoryFor<CourseLecturer>().Add(CourseLecturer);
            this.uow.Save();
        }

        public void AddOrEditCourseLecturer(CourseLecturer CourseLecturer)
        {
            if (CourseLecturer.ID == Guid.Empty)
            {
                this.AddCourseLecturer(CourseLecturer);
            }
            else
            {
                this.EditCourseLecturer(CourseLecturer);
            }
        }

        public void EditCourseLecturer(CourseLecturer CourseLecturer)
        {
            this.uow.RepositoryFor<CourseLecturer>().Edit(CourseLecturer);
            this.uow.Save();
        }

        public IEnumerable<CourseLecturer> GetAllCourseLecturer() => 
            (from a in this.GetAllCourseLecturerIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<CourseLecturer> GetAllCourseLecturerIncludingDeleted() => 
            this.uow.RepositoryFor<CourseLecturer>().GetAll();

        public CourseLecturer GetByKey(Guid key) => 
            (from a in this.GetAllCourseLecturer()
                where a.Key == key
                select a).FirstOrDefault<CourseLecturer>();

        public CourseLecturer GetCourseLecturer(Guid id) => 
            this.uow.RepositoryFor<CourseLecturer>().Get(id);

        public void RemoveCourseLecturer(Guid id)
        {
            CourseLecturer courseLecturer = this.GetCourseLecturer(id);
            courseLecturer.IsDelete = true;
            this.EditCourseLecturer(courseLecturer);
        }

        public double TotalCourseLecturer() => 
            ((double) this.GetAllCourseLecturer().Count<CourseLecturer>());

       
    }
}

