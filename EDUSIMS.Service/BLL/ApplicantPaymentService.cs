﻿namespace EDUSIMS.Service.BLL
{
    using EDUSIMS.Core.Model;
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class ApplicantPaymentService : IApplicantPaymentService
    {
        private EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork uow = new EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork();

        public void AddApplicantPayment(ApplicantPayment ApplicantPayment)
        {
            this.uow.RepositoryFor<ApplicantPayment>().Add(ApplicantPayment);
            this.uow.Save();
        }

        public void AddOrEditApplicantPayment(ApplicantPayment ApplicantPayment)
        {
            if (ApplicantPayment.ID == Guid.Empty)
            {
                this.AddApplicantPayment(ApplicantPayment);
            }
            else
            {
                this.EditApplicantPayment(ApplicantPayment);
            }
        }

        public void EditApplicantPayment(ApplicantPayment ApplicantPayment)
        {
            this.uow.RepositoryFor<ApplicantPayment>().Edit(ApplicantPayment);
            this.uow.Save();
        }

        public IEnumerable<ApplicantPayment> GetAllApplicantPayment() => 
            (from a in this.GetAllApplicantPaymentIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<ApplicantPayment> GetAllApplicantPaymentIncludingDeleted() => 
            this.uow.RepositoryFor<ApplicantPayment>().GetAll();

        public ApplicantPayment GetApplicantPayment(Guid id) => 
            this.uow.RepositoryFor<ApplicantPayment>().Get(id);

        public ApplicantPayment GetByKey(Guid key) => 
            (from a in this.GetAllApplicantPayment()
                where a.Key == key
                select a).FirstOrDefault<ApplicantPayment>();

        public void RemoveApplicantPayment(Guid id)
        {
            ApplicantPayment applicantPayment = this.GetApplicantPayment(id);
            applicantPayment.IsDelete = true;
            this.EditApplicantPayment(applicantPayment);
        }

        public double TotalApplicantPayment() => 
            ((double) this.GetAllApplicantPayment().Count<ApplicantPayment>());

        
       
    }
}

