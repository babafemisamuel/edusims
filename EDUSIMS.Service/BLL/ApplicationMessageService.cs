﻿namespace EDUSIMS.Service.BLL
{
    using EDUSIMS.Core.Model;
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public class ApplicationMessageService : IApplicationMessageService
    {
        private EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork uow = new EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork();

        public void AddApplicationMessage(ApplicationMessage ApplicationMessage)
        {
            this.uow.RepositoryFor<ApplicationMessage>().Add(ApplicationMessage);
            this.uow.Save();
        }

        public void AddOrEditApplicationMessage(ApplicationMessage ApplicationMessage)
        {
            if (ApplicationMessage.ID == Guid.Empty)
            {
                this.AddApplicationMessage(ApplicationMessage);
            }
            else
            {
                this.EditApplicationMessage(ApplicationMessage);
            }
        }

        public void EditApplicationMessage(ApplicationMessage ApplicationMessage)
        {
            this.uow.RepositoryFor<ApplicationMessage>().Edit(ApplicationMessage);
            this.uow.Save();
        }

        public IEnumerable<ApplicationMessage> GetAllApplicationMessage() => 
            (from a in this.GetAllApplicationMessageIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<ApplicationMessage> GetAllApplicationMessageIncludingDeleted() => 
            this.uow.RepositoryFor<ApplicationMessage>().GetAll();

        public ApplicationMessage GetApplicationMessage(Guid id) => 
            this.uow.RepositoryFor<ApplicationMessage>().Get(id);

        public ApplicationMessage GetByKey(Guid key) => 
            (from a in this.GetAllApplicationMessage()
                where a.Key == key
                select a).FirstOrDefault<ApplicationMessage>();

        public void RemoveApplicationMessage(Guid id)
        {
            ApplicationMessage applicationMessage = this.GetApplicationMessage(id);
            applicationMessage.IsDelete = true;
            this.EditApplicationMessage(applicationMessage);
        }

        public double TotalApplicationMessage() => 
            ((double) this.GetAllApplicationMessage().Count<ApplicationMessage>());

       
    }
}

