﻿namespace EDUSIMS.Service.BLL
{
    using EDUSIMS.Core.Model;
    using EDUSIMS.Infrastructure.UnitOfWork;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public class AcademicSessionService : IAcademicSessionService
    {
        private UnitOfWork uow = new UnitOfWork();

        public void AddAcademicSession(AcademicSession AcademicSession)
        {
            this.uow.RepositoryFor<AcademicSession>().Add(AcademicSession);
            this.uow.Save();
        }

        public void AddOrEditAcademicSession(AcademicSession AcademicSession)
        {
            if (AcademicSession.ID == Guid.Empty)
            {
                this.AddAcademicSession(AcademicSession);
            }
            else
            {
                this.EditAcademicSession(AcademicSession);
            }
        }

        public void EditAcademicSession(AcademicSession AcademicSession)
        {
            this.uow.RepositoryFor<AcademicSession>().Edit(AcademicSession);
            this.uow.Save();
        }

        public AcademicSession GetAcademicSession(Guid id) => 
            this.uow.RepositoryFor<AcademicSession>().Get(id);

        public IEnumerable<AcademicSession> GetAllAcademicSession() => 
            (from a in this.GetAllAcademicSessionIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<AcademicSession> GetAllAcademicSessionIncludingDeleted() => 
            this.uow.RepositoryFor<AcademicSession>().GetAll();

        public AcademicSession GetByKey(Guid key) => 
            (from a in this.GetAllAcademicSession()
                where a.Key == key
                select a).FirstOrDefault<AcademicSession>();

        public void RemoveAcademicSession(Guid id)
        {
            AcademicSession academicSession = this.GetAcademicSession(id);
            academicSession.IsDelete = true;
            this.EditAcademicSession(academicSession);
        }

        public double TotalAcademicSession() => 
            ((double) this.GetAllAcademicSession().Count<AcademicSession>());

       
    }
}

