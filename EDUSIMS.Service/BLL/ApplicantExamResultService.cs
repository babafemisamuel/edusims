﻿namespace EDUSIMS.Service.BLL
{
  
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using EDUSIMS.Core.Model;

    public class ApplicantExamResultService : IApplicantExamResultService
    {
        private EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork uow = new EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork();

        public void AddApplicantExamResult(ApplicantExamResult ApplicantExamResult)
        {
            this.uow.RepositoryFor<ApplicantExamResult>().Add(ApplicantExamResult);
            this.uow.Save();
        }

        public void AddOrEditApplicantExamResult(ApplicantExamResult ApplicantExamResult)
        {
            if (ApplicantExamResult.ID == Guid.Empty)
            {
                this.AddApplicantExamResult(ApplicantExamResult);
            }
            else
            {
                this.EditApplicantExamResult(ApplicantExamResult);
            }
        }

        public void EditApplicantExamResult(ApplicantExamResult ApplicantExamResult)
        {
            this.uow.RepositoryFor<ApplicantExamResult>().Edit(ApplicantExamResult);
            this.uow.Save();
        }

        public IEnumerable<ApplicantExamResult> GetAllApplicantExamResult() => 
            (from a in this.GetAllApplicantExamResultIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<ApplicantExamResult> GetAllApplicantExamResultIncludingDeleted() => 
            this.uow.RepositoryFor<ApplicantExamResult>().GetAll();

        public ApplicantExamResult GetApplicantExamResult(Guid id) => 
            this.uow.RepositoryFor<ApplicantExamResult>().Get(id);

        public ApplicantExamResult GetByKey(Guid key) => 
            (from a in this.GetAllApplicantExamResult()
                where a.Key == key
                select a).FirstOrDefault<ApplicantExamResult>();

        public void RemoveApplicantExamResult(Guid id)
        {
            ApplicantExamResult applicantExamResult = this.GetApplicantExamResult(id);
            applicantExamResult.IsDelete = true;
            this.EditApplicantExamResult(applicantExamResult);
        }

        public double TotalApplicantExamResult() => 
            ((double) this.GetAllApplicantExamResult().Count<ApplicantExamResult>());

       
    }
}

