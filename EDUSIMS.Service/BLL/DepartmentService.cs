﻿namespace EDUSIMS.Service.BLL
{
    using EDUSIMS.Core.Model;
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public class DepartmentService : IDepartmentService
    {
        private EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork uow = new EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork();

        public void AddDepartment(Department Department)
        {
            this.uow.RepositoryFor<Department>().Add(Department);
            this.uow.Save();
        }

        public void AddOrEditDepartment(Department Department)
        {
            if (Department.ID == Guid.Empty)
            {
                this.AddDepartment(Department);
            }
            else
            {
                this.EditDepartment(Department);
            }
        }

        public void EditDepartment(Department Department)
        {
            this.uow.RepositoryFor<Department>().Edit(Department);
            this.uow.Save();
        }

        public IEnumerable<Department> GetAllDepartment() => 
            (from a in this.GetAllDepartmentIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<Department> GetAllDepartmentIncludingDeleted() => 
            this.uow.RepositoryFor<Department>().GetAll();

        public Department GetByKey(Guid key) => 
            (from a in this.GetAllDepartment()
                where a.Key == key
                select a).FirstOrDefault<Department>();

        public Department GetDepartment(Guid id) => 
            this.uow.RepositoryFor<Department>().Get(id);

        public void RemoveDepartment(Guid id)
        {
            Department department = this.GetDepartment(id);
            department.IsDelete = true;
            this.EditDepartment(department);
        }

        public double TotalDepartment() => 
            ((double) this.GetAllDepartment().Count<Department>());

        
    }
}

