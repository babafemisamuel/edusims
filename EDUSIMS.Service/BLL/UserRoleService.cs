﻿namespace EDUSIMS.Service.BLL
{
    using EDUSIMS.Core.Model;
    using EDUSIMS.Infrastructure.UnitOfWork;
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;


    public class UserRoleService : IUserRoleService
    {
        private EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork uow = new EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork();

        public void AddOrEditUserRole(UserRole UserRole)
        {
            if (UserRole.ID == Guid.Empty)
            {
                this.AddUserRole(UserRole);
            }
            else
            {
                this.EditUserRole(UserRole);
            }
        }

        public void AddUserRole(UserRole UserRole)
        {
            this.uow.RepositoryFor<UserRole>().Add(UserRole);
            this.uow.Save();
        }

        public void EditUserRole(UserRole UserRole)
        {
            this.uow.RepositoryFor<UserRole>().Edit(UserRole);
            this.uow.Save();
        }

        public IEnumerable<UserRole> GetAllUserRole() => 
            (from a in this.GetAllUserRoleIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<UserRole> GetAllUserRoleIncludingDeleted() => 
            this.uow.RepositoryFor<UserRole>().GetAll();

        public UserRole GetByKey(Guid key) => 
            (from a in this.GetAllUserRole()
                where a.Key == key
                select a).FirstOrDefault<UserRole>();

        public UserRole GetUserRole(Guid id) => 
            this.uow.RepositoryFor<UserRole>().Get(id);

        public void RemoveUserRole(Guid id)
        {
            UserRole userRole = this.GetUserRole(id);
            userRole.IsDelete = true;
            this.EditUserRole(userRole);
        }

        public double TotalUserRole() => 
            ((double) this.GetAllUserRole().Count<UserRole>());

       
    }
}

