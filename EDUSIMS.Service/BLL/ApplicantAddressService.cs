﻿namespace EDUSIMS.Service.BLL
{
 
    using EDUSIMS.Infrastructure.UnitOfWork;
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using EDUSIMS.Core.Model;

    public class ApplicantAddressService : IApplicantAddressService
    {
        private UnitOfWork uow = new UnitOfWork();

        public void AddApplicantAddress(ApplicantAddress ApplicantAddress)
        {
            this.uow.RepositoryFor<ApplicantAddress>().Add(ApplicantAddress);
            this.uow.Save();
        }

        public void AddOrEditApplicantAddress(ApplicantAddress ApplicantAddress)
        {
            if (ApplicantAddress.ID == Guid.Empty)
            {
                this.AddApplicantAddress(ApplicantAddress);
            }
            else
            {
                this.EditApplicantAddress(ApplicantAddress);
            }
        }

        public void EditApplicantAddress(ApplicantAddress ApplicantAddress)
        {
            this.uow.RepositoryFor<ApplicantAddress>().Edit(ApplicantAddress);
            this.uow.Save();
        }

        public IEnumerable<ApplicantAddress> GetAllApplicantAddress() => 
            (from a in this.GetAllApplicantAddressIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<ApplicantAddress> GetAllApplicantAddressIncludingDeleted() => 
            this.uow.RepositoryFor<ApplicantAddress>().GetAll();

        public ApplicantAddress GetApplicantAddress(Guid id) => 
            this.uow.RepositoryFor<ApplicantAddress>().Get(id);

        public ApplicantAddress GetByKey(Guid key) => 
            (from a in this.GetAllApplicantAddress()
                where a.Key == key
                select a).FirstOrDefault<ApplicantAddress>();

        public void RemoveApplicantAddress(Guid id)
        {
            ApplicantAddress applicantAddress = this.GetApplicantAddress(id);
            applicantAddress.IsDelete = true;
            this.EditApplicantAddress(applicantAddress);
        }

        public double TotalApplicantAddress() => 
            ((double) this.GetAllApplicantAddress().Count<ApplicantAddress>());

       
    }
}

