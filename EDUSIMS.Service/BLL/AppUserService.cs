﻿namespace EDUSIMS.Service.BLL
{
    using EDUSIMS.Core.Model;
    using EDUSIMS.Infrastructure.UnitOfWork;
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public class AppUserService : IAppUserService
    {
        private EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork uow = new EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork();

        public void AddAppUser(AppUser AppUser)
        {
            this.uow.RepositoryFor<AppUser>().Add(AppUser);
            this.uow.Save();
        }

        public void AddOrEditAppUser(AppUser AppUser)
        {
            if (AppUser.ID == Guid.Empty)
            {
                this.AddAppUser(AppUser);
            }
            else
            {
                this.EditAppUser(AppUser);
            }
        }

        public void EditAppUser(AppUser AppUser)
        {
            this.uow.RepositoryFor<AppUser>().Edit(AppUser);
            this.uow.Save();
        }

        public IEnumerable<AppUser> GetAllAppUser() => 
            (from a in this.GetAllAppUserIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<AppUser> GetAllAppUserIncludingDeleted() => 
            this.uow.RepositoryFor<AppUser>().GetAll();

        public AppUser GetAppUser(Guid id) => 
            this.uow.RepositoryFor<AppUser>().Get(id);

        public AppUser GetByKey(Guid key) => 
            (from a in this.GetAllAppUser()
                where a.Key == key
                select a).FirstOrDefault<AppUser>();

        public void RemoveAppUser(Guid id)
        {
            AppUser appUser = this.GetAppUser(id);
            appUser.IsDelete = true;
            this.EditAppUser(appUser);
        }

        public double TotalAppUser() => 
            ((double) this.GetAllAppUser().Count<AppUser>());

       
    }
}

