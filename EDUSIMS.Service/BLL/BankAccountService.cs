﻿namespace EDUSIMS.Service.BLL
{
    using EDUSIMS.Core.Model;
    using EDUSIMS.Infrastructure.UnitOfWork;
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public class BankAccountService : IBankAccountService
    {
        private EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork uow = new EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork();

        public void AddBankAccount(BankAccount BankAccount)
        {
            this.uow.RepositoryFor<BankAccount>().Add(BankAccount);
            this.uow.Save();
        }

        public void AddOrEditBankAccount(BankAccount BankAccount)
        {
            if (BankAccount.ID == Guid.Empty)
            {
                this.AddBankAccount(BankAccount);
            }
            else
            {
                this.EditBankAccount(BankAccount);
            }
        }

        public void EditBankAccount(BankAccount BankAccount)
        {
            this.uow.RepositoryFor<BankAccount>().Edit(BankAccount);
            this.uow.Save();
        }

        public IEnumerable<BankAccount> GetAllBankAccount() => 
            (from a in this.GetAllBankAccountIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<BankAccount> GetAllBankAccountIncludingDeleted() => 
            this.uow.RepositoryFor<BankAccount>().GetAll();

        public BankAccount GetBankAccount(Guid id) => 
            this.uow.RepositoryFor<BankAccount>().Get(id);

        public BankAccount GetByKey(Guid key) => 
            (from a in this.GetAllBankAccount()
                where a.Key == key
                select a).FirstOrDefault<BankAccount>();

        public void RemoveBankAccount(Guid id)
        {
            BankAccount bankAccount = this.GetBankAccount(id);
            bankAccount.IsDelete = true;
            this.EditBankAccount(bankAccount);
        }

        public double TotalBankAccount() => 
            ((double) this.GetAllBankAccount().Count<BankAccount>());

        
    }
}

