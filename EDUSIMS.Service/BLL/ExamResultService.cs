﻿namespace EDUSIMS.Service.BLL
{
    using EDUSIMS.Core.Model;
    using EDUSIMS.Infrastructure.UnitOfWork;
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using EDUSIMS.Core.Model;

    public class ExamResultService : IExamResultService
    {
        private EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork uow = new EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork();

        public void AddExamResult(ExamResult ExamResult)
        {
            this.uow.RepositoryFor<ExamResult>().Add(ExamResult);
            this.uow.Save();
        }

        public void AddOrEditExamResult(ExamResult ExamResult)
        {
            if (ExamResult.ID == Guid.Empty)
            {
                this.AddExamResult(ExamResult);
            }
            else
            {
                this.EditExamResult(ExamResult);
            }
        }

        public void EditExamResult(ExamResult ExamResult)
        {
            this.uow.RepositoryFor<ExamResult>().Edit(ExamResult);
            this.uow.Save();
        }

        public IEnumerable<ExamResult> GetAllExamResult() => 
            (from a in this.GetAllExamResultIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<ExamResult> GetAllExamResultIncludingDeleted() => 
            this.uow.RepositoryFor<ExamResult>().GetAll();

        public ExamResult GetByKey(Guid key) => 
            (from a in this.GetAllExamResult()
                where a.Key == key
                select a).FirstOrDefault<ExamResult>();

        public ExamResult GetExamResult(Guid id) => 
            this.uow.RepositoryFor<ExamResult>().Get(id);

        public void RemoveExamResult(Guid id)
        {
            ExamResult examResult = this.GetExamResult(id);
            examResult.IsDelete = true;
            this.EditExamResult(examResult);
        }

        public double TotalExamResult() => 
            ((double) this.GetAllExamResult().Count<ExamResult>());

      
    }
}

