﻿namespace EDUSIMS.Service.BLL
{
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using EDUSIMS.Core.Model;

    public class PaymentInstructionService : IPaymentInstructionService
    {
        private EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork uow = new EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork();

        public void AddOrEditPaymentInstruction(PaymentInstruction PaymentInstruction)
        {
            if (PaymentInstruction.ID == Guid.Empty)
            {
                this.AddPaymentInstruction(PaymentInstruction);
            }
            else
            {
                this.EditPaymentInstruction(PaymentInstruction);
            }
        }

        public void AddPaymentInstruction(PaymentInstruction PaymentInstruction)
        {
            this.uow.RepositoryFor<PaymentInstruction>().Add(PaymentInstruction);
            this.uow.Save();
        }

        public void EditPaymentInstruction(PaymentInstruction PaymentInstruction)
        {
            this.uow.RepositoryFor<PaymentInstruction>().Edit(PaymentInstruction);
            this.uow.Save();
        }

        public IEnumerable<PaymentInstruction> GetAllPaymentInstruction() => 
            (from a in this.GetAllPaymentInstructionIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<PaymentInstruction> GetAllPaymentInstructionIncludingDeleted() => 
            this.uow.RepositoryFor<PaymentInstruction>().GetAll();

        public PaymentInstruction GetByKey(Guid key) => 
            (from a in this.GetAllPaymentInstruction()
                where a.Key == key
                select a).FirstOrDefault<PaymentInstruction>();

        public PaymentInstruction GetPaymentInstruction(Guid id) => 
            this.uow.RepositoryFor<PaymentInstruction>().Get(id);

        public void RemovePaymentInstruction(Guid id)
        {
            PaymentInstruction paymentInstruction = this.GetPaymentInstruction(id);
            paymentInstruction.IsDelete = true;
            this.EditPaymentInstruction(paymentInstruction);
        }

        public double TotalPaymentInstruction() => 
            ((double) this.GetAllPaymentInstruction().Count<PaymentInstruction>());

        
    }
}

