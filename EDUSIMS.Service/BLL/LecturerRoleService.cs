﻿namespace EDUSIMS.Service.BLL
{
    using EDUSIMS.Core.Model;
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public class LecturerRoleService : ILecturerRoleService
    {
        private EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork uow = new EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork();

        public void AddLecturerRole(LecturerRole LecturerRole)
        {
            this.uow.RepositoryFor<LecturerRole>().Add(LecturerRole);
            this.uow.Save();
        }

        public void AddOrEditLecturerRole(LecturerRole LecturerRole)
        {
            if (LecturerRole.ID == Guid.Empty)
            {
                this.AddLecturerRole(LecturerRole);
            }
            else
            {
                this.EditLecturerRole(LecturerRole);
            }
        }

        public void EditLecturerRole(LecturerRole LecturerRole)
        {
            this.uow.RepositoryFor<LecturerRole>().Edit(LecturerRole);
            this.uow.Save();
        }

        public IEnumerable<LecturerRole> GetAllLecturerRole() => 
            (from a in this.GetAllLecturerRoleIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<LecturerRole> GetAllLecturerRoleIncludingDeleted() => 
            this.uow.RepositoryFor<LecturerRole>().GetAll();

        public LecturerRole GetByKey(Guid key) => 
            (from a in this.GetAllLecturerRole()
                where a.Key == key
                select a).FirstOrDefault<LecturerRole>();

        public LecturerRole GetLecturerRole(Guid id) => 
            this.uow.RepositoryFor<LecturerRole>().Get(id);

        public void RemoveLecturerRole(Guid id)
        {
            LecturerRole lecturerRole = this.GetLecturerRole(id);
            lecturerRole.IsDelete = true;
            this.EditLecturerRole(lecturerRole);
        }

        public double TotalLecturerRole() => 
            ((double) this.GetAllLecturerRole().Count<LecturerRole>());

       
    }
}

