﻿namespace EDUSIMS.Service.BLL
{
    using EDUSIMS.Core.Model;
    using EDUSIMS.Infrastructure.UnitOfWork;
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public class CourseService : ICourseService
    {
        private EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork uow = new EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork();

        public void AddCourse(EDUSIMS.Core.Model.Course Course)
        {
            this.uow.RepositoryFor<Course>().Add(Course);
            this.uow.Save();
        }

        public void AddOrEditCourse(Course Course)
        {
            if (Course.ID == Guid.Empty)
            {
                this.AddCourse(Course);
            }
            else
            {
                this.EditCourse(Course);
            }
        }

        public void EditCourse(Course Course)
        {
            this.uow.RepositoryFor<Course>().Edit(Course);
            this.uow.Save();
        }

        public IEnumerable<Course> GetAllCourse() => 
            (from a in this.GetAllCourseIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<Course> GetAllCourseIncludingDeleted() => 
            this.uow.RepositoryFor<Course>().GetAll();

        public Course GetByKey(Guid key) => 
            (from a in this.GetAllCourse()
                where a.Key == key
                select a).FirstOrDefault<Course>();

        public Course GetCourse(Guid id) => 
            this.uow.RepositoryFor<Course>().Get(id);

        public void RemoveCourse(Guid id)
        {
            Course course = this.GetCourse(id);
            course.IsDelete = true;
            this.EditCourse(course);
        }

        public double TotalCourse() => 
            ((double) this.GetAllCourse().Count<Course>());

        
    }
}

