﻿namespace EDUSIMS.Service.BLL
{
    using EDUSIMS.Core.Model;
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public class ProgrammeService : IProgrammeService
    {
        private EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork uow = new EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork();

        public void AddOrEditProgramme(Programme Programme)
        {
            if (Programme.ID == Guid.Empty)
            {
                this.AddProgramme(Programme);
            }
            else
            {
                this.EditProgramme(Programme);
            }
        }

        public void AddProgramme(Programme Programme)
        {
            this.uow.RepositoryFor<Programme>().Add(Programme);
            this.uow.Save();
        }

        public void EditProgramme(Programme Programme)
        {
            this.uow.RepositoryFor<Programme>().Edit(Programme);
            this.uow.Save();
        }

        public IEnumerable<Programme> GetAllProgramme() => 
            (from a in this.GetAllProgrammeIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<Programme> GetAllProgrammeIncludingDeleted() => 
            this.uow.RepositoryFor<Programme>().GetAll();

        public Programme GetByKey(Guid key) => 
            (from a in this.GetAllProgramme()
                where a.Key == key
                select a).FirstOrDefault<Programme>();

        public Programme GetProgramme(Guid id) => 
            this.uow.RepositoryFor<Programme>().Get(id);

        public void RemoveProgramme(Guid id)
        {
            Programme programme = this.GetProgramme(id);
            programme.IsDelete = true;
            this.EditProgramme(programme);
        }

        public double TotalProgramme() => 
            ((double) this.GetAllProgramme().Count<Programme>());

       
    }
}

