﻿namespace EDUSIMS.Service.BLL
{
    using EDUSIMS.Core.Model;
    using EDUSIMS.Infrastructure.UnitOfWork;
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using EDUSIMS.Core.Model;

    public class GradeConfigService : IGradeConfigService
    {
        private EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork uow = new EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork();

        public void AddGradeConfig(GradeConfig GradeConfig)
        {
            this.uow.RepositoryFor<GradeConfig>().Add(GradeConfig);
            this.uow.Save();
        }

        public void AddOrEditGradeConfig(GradeConfig GradeConfig)
        {
            if (GradeConfig.ID == Guid.Empty)
            {
                this.AddGradeConfig(GradeConfig);
            }
            else
            {
                this.EditGradeConfig(GradeConfig);
            }
        }

        public void EditGradeConfig(GradeConfig GradeConfig)
        {
            this.uow.RepositoryFor<GradeConfig>().Edit(GradeConfig);
            this.uow.Save();
        }

        public IEnumerable<GradeConfig> GetAllGradeConfig() => 
            (from a in this.GetAllGradeConfigIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<GradeConfig> GetAllGradeConfigIncludingDeleted() => 
            this.uow.RepositoryFor<GradeConfig>().GetAll();

        public GradeConfig GetByKey(Guid key) => 
            (from a in this.GetAllGradeConfig()
                where a.Key == key
                select a).FirstOrDefault<GradeConfig>();

        public GradeConfig GetGradeConfig(Guid id) => 
            this.uow.RepositoryFor<GradeConfig>().Get(id);

        public void RemoveGradeConfig(Guid id)
        {
            GradeConfig gradeConfig = this.GetGradeConfig(id);
            gradeConfig.IsDelete = true;
            this.EditGradeConfig(gradeConfig);
        }

        public double TotalGradeConfig() => 
            ((double) this.GetAllGradeConfig().Count<GradeConfig>());

        
    }
}

