﻿namespace EDUSIMS.Service.BLL
{
    using EDUSIMS.Core.Model;
    using EDUSIMS.Infrastructure.UnitOfWork;
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using EDUSIMS.Core.Model;

    public class StudentCourseService : IStudentCourseService
    {
        private EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork uow = new EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork();

        public void AddOrEditStudentCourse(StudentCourse StudentCourse)
        {
            if (StudentCourse.ID == Guid.Empty)
            {
                this.AddStudentCourse(StudentCourse);
            }
            else
            {
                this.EditStudentCourse(StudentCourse);
            }
        }

        public void AddStudentCourse(StudentCourse StudentCourse)
        {
            this.uow.RepositoryFor<StudentCourse>().Add(StudentCourse);
            this.uow.Save();
        }

        public void EditStudentCourse(StudentCourse StudentCourse)
        {
            this.uow.RepositoryFor<StudentCourse>().Edit(StudentCourse);
            this.uow.Save();
        }

        public IEnumerable<StudentCourse> GetAllStudentCourse() => 
            (from a in this.GetAllStudentCourseIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<StudentCourse> GetAllStudentCourseIncludingDeleted() => 
            this.uow.RepositoryFor<StudentCourse>().GetAll();

        public StudentCourse GetByKey(Guid key) => 
            (from a in this.GetAllStudentCourse()
                where a.Key == key
                select a).FirstOrDefault<StudentCourse>();

        public StudentCourse GetStudentCourse(Guid id) => 
            this.uow.RepositoryFor<StudentCourse>().Get(id);

        public void RemoveStudentCourse(Guid id)
        {
            StudentCourse studentCourse = this.GetStudentCourse(id);
            studentCourse.IsDelete = true;
            this.EditStudentCourse(studentCourse);
        }

        public double TotalStudentCourse() => 
            ((double) this.GetAllStudentCourse().Count<StudentCourse>());

        
    }
}

