﻿namespace EDUSIMS.Service.BLL
{
    using EDUSIMS.Core.Model;
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class ApplicantStudentService : IApplicantStudentService
    {
        private EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork uow = new EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork();

        public void AddApplicantStudent(ApplicantStudent ApplicantStudent)
        {
            this.uow.RepositoryFor<ApplicantStudent>().Add(ApplicantStudent);
            this.uow.Save();
        }

        public void AddOrEditApplicantStudent(ApplicantStudent ApplicantStudent)
        {
            if (ApplicantStudent.ID == Guid.Empty)
            {
                this.AddApplicantStudent(ApplicantStudent);
            }
            else
            {
                this.EditApplicantStudent(ApplicantStudent);
            }
        }

        public void EditApplicantStudent(ApplicantStudent ApplicantStudent)
        {
            this.uow.RepositoryFor<ApplicantStudent>().Edit(ApplicantStudent);
            this.uow.Save();
        }

        public IEnumerable<ApplicantStudent> GetAllApplicantStudent() => 
            (from a in this.GetAllApplicantStudentIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<ApplicantStudent> GetAllApplicantStudentIncludingDeleted() => 
            this.uow.RepositoryFor<ApplicantStudent>().GetAll();

        public ApplicantStudent GetApplicantStudent(Guid id) => 
            this.uow.RepositoryFor<ApplicantStudent>().Get(id);

        public ApplicantStudent GetByKey(Guid key) => 
            (from a in this.GetAllApplicantStudent()
                where a.Key == key
                select a).FirstOrDefault<ApplicantStudent>();

        public void RemoveApplicantStudent(Guid id)
        {
            ApplicantStudent applicantStudent = this.GetApplicantStudent(id);
            applicantStudent.IsDelete = true;
            this.EditApplicantStudent(applicantStudent);
        }

        public double TotalApplicantStudent() => 
            ((double) this.GetAllApplicantStudent().Count<ApplicantStudent>());

       
    }
}

