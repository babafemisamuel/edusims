﻿namespace EDUSIMS.Service.BLL
{
    using EDUSIMS.Infrastructure.UnitOfWork;
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using EDUSIMS.Core.Model;

    public class SchoolService : ISchoolService
    {
        private EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork uow = new EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork();

        public void AddOrEditSchool(School School)
        {
            if (School.ID == Guid.Empty)
            {
                this.AddSchool(School);
            }
            else
            {
                this.EditSchool(School);
            }
        }

        public void AddSchool(School School)
        {
            this.uow.RepositoryFor<School>().Add(School);
            this.uow.Save();
        }

        public void EditSchool(School School)
        {
            this.uow.RepositoryFor<School>().Edit(School);
            this.uow.Save();
        }

        public IEnumerable<School> GetAllSchool() => 
            (from a in this.GetAllSchoolIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<School> GetAllSchoolIncludingDeleted() => 
            this.uow.RepositoryFor<School>().GetAll();

        public School GetByKey(Guid key) => 
            (from a in this.GetAllSchool()
                where a.Key == key
                select a).FirstOrDefault<School>();

        public School GetSchool(Guid id) => 
            this.uow.RepositoryFor<School>().Get(id);

        public void RemoveSchool(Guid id)
        {
            School school = this.GetSchool(id);
            school.IsDelete = true;
            this.EditSchool(school);
        }

        public double TotalSchool() => 
            ((double) this.GetAllSchool().Count<School>());

        
    }
}

