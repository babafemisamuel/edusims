﻿namespace EDUSIMS.Service.BLL
{
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using EDUSIMS.Core.Model;

    public class ApplicantEducationHistoriesService : IApplicantEducationHistoriesService
    {
        private EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork uow = new EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork();

        public void AddApplicantEducationHistory(ApplicantEducationHistory ApplicantEducationHistory)
        {
            this.uow.RepositoryFor<ApplicantEducationHistory>().Add(ApplicantEducationHistory);
            this.uow.Save();
        }

        public void AddOrEditApplicantEducationHistory(ApplicantEducationHistory ApplicantEducationHistory)
        {
            if (ApplicantEducationHistory.ID == Guid.Empty)
            {
                this.AddApplicantEducationHistory(ApplicantEducationHistory);
            }
            else
            {
                this.EditApplicantEducationHistory(ApplicantEducationHistory);
            }
        }

        public void EditApplicantEducationHistory(ApplicantEducationHistory ApplicantEducationHistory)
        {
            this.uow.RepositoryFor<ApplicantEducationHistory>().Edit(ApplicantEducationHistory);
            this.uow.Save();
        }

        public IEnumerable<ApplicantEducationHistory> GetAllApplicantEducationHistory() => 
            (from a in this.GetAllApplicantEducationHistoryIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<ApplicantEducationHistory> GetAllApplicantEducationHistoryIncludingDeleted() => 
            this.uow.RepositoryFor<ApplicantEducationHistory>().GetAll();

        public ApplicantEducationHistory GetApplicantEducationHistory(Guid id) => 
            this.uow.RepositoryFor<ApplicantEducationHistory>().Get(id);

        public ApplicantEducationHistory GetByKey(Guid key) => 
            (from a in this.GetAllApplicantEducationHistory()
                where a.Key == key
                select a).FirstOrDefault<ApplicantEducationHistory>();

        public void RemoveApplicantEducationHistory(Guid id)
        {
            ApplicantEducationHistory applicantEducationHistory = this.GetApplicantEducationHistory(id);
            applicantEducationHistory.IsDelete = true;
            this.EditApplicantEducationHistory(applicantEducationHistory);
        }

        public double TotalApplicantEducationHistory() => 
            ((double) this.GetAllApplicantEducationHistory().Count<ApplicantEducationHistory>());

      
    }
}

