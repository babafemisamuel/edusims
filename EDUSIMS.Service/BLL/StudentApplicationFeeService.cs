﻿namespace EDUSIMS.Service.BLL
{
    using EDUSIMS.Infrastructure.UnitOfWork;
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using EDUSIMS.Core.Model;

    public class StudentApplicationFeeService : IStudentApplicationFeeService
    {
        private EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork uow = new EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork();

        public void AddOrEditStudentApplicationFee(StudentApplicationFee StudentApplicationFee)
        {
            if (StudentApplicationFee.ID == Guid.Empty)
            {
                this.AddStudentApplicationFee(StudentApplicationFee);
            }
            else
            {
                this.EditStudentApplicationFee(StudentApplicationFee);
            }
        }

        public void AddStudentApplicationFee(StudentApplicationFee StudentApplicationFee)
        {
            this.uow.RepositoryFor<StudentApplicationFee>().Add(StudentApplicationFee);
            this.uow.Save();
        }

        public void EditStudentApplicationFee(StudentApplicationFee StudentApplicationFee)
        {
            this.uow.RepositoryFor<StudentApplicationFee>().Edit(StudentApplicationFee);
            this.uow.Save();
        }

        public IEnumerable<StudentApplicationFee> GetAllStudentApplicationFee() => 
            (from a in this.GetAllStudentApplicationFeeIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<StudentApplicationFee> GetAllStudentApplicationFeeIncludingDeleted() => 
            this.uow.RepositoryFor<StudentApplicationFee>().GetAll();

        public StudentApplicationFee GetByKey(Guid key) => 
            (from a in this.GetAllStudentApplicationFee()
                where a.Key == key
                select a).FirstOrDefault<StudentApplicationFee>();

        public StudentApplicationFee GetStudentApplicationFee(Guid id) => 
            this.uow.RepositoryFor<StudentApplicationFee>().Get(id);

        public void RemoveStudentApplicationFee(Guid id)
        {
            StudentApplicationFee studentApplicationFee = this.GetStudentApplicationFee(id);
            studentApplicationFee.IsDelete = true;
            this.EditStudentApplicationFee(studentApplicationFee);
        }

        public double TotalStudentApplicationFee() => 
            ((double) this.GetAllStudentApplicationFee().Count<StudentApplicationFee>());

        
    }
}

