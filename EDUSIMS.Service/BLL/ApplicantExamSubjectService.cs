﻿namespace EDUSIMS.Service.BLL
{
   
    using EDUSIMS.Infrastructure.UnitOfWork;
    using EDUSIMS.Service.Interface;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using EDUSIMS.Core.Model;

    public class ApplicantExamSubjectService : IApplicantExamSubjectService
    {
        private EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork uow = new EDUSIMS.Infrastructure.UnitOfWork.UnitOfWork();

        public void AddApplicantExamSubject(ApplicantExamSubject ApplicantExamSubject)
        {
            this.uow.RepositoryFor<ApplicantExamSubject>().Add(ApplicantExamSubject);
            this.uow.Save();
        }

        public void AddOrEditApplicantExamSubject(ApplicantExamSubject ApplicantExamSubject)
        {
            if (ApplicantExamSubject.ID == Guid.Empty)
            {
                this.AddApplicantExamSubject(ApplicantExamSubject);
            }
            else
            {
                this.EditApplicantExamSubject(ApplicantExamSubject);
            }
        }

        public void EditApplicantExamSubject(ApplicantExamSubject ApplicantExamSubject)
        {
            this.uow.RepositoryFor<ApplicantExamSubject>().Edit(ApplicantExamSubject);
            this.uow.Save();
        }

        public IEnumerable<ApplicantExamSubject> GetAllApplicantExamSubject() => 
            (from a in this.GetAllApplicantExamSubjectIncludingDeleted()
                where !a.IsDelete
                select a);

        public IEnumerable<ApplicantExamSubject> GetAllApplicantExamSubjectIncludingDeleted() => 
            this.uow.RepositoryFor<ApplicantExamSubject>().GetAll();

        public ApplicantExamSubject GetApplicantExamSubject(Guid id) => 
            this.uow.RepositoryFor<ApplicantExamSubject>().Get(id);

        public ApplicantExamSubject GetByKey(Guid key) => 
            (from a in this.GetAllApplicantExamSubject()
                where a.Key == key
                select a).FirstOrDefault<ApplicantExamSubject>();

        public void RemoveApplicantExamSubject(Guid id)
        {
            ApplicantExamSubject applicantExamSubject = this.GetApplicantExamSubject(id);
            applicantExamSubject.IsDelete = true;
            this.EditApplicantExamSubject(applicantExamSubject);
        }

        public double TotalApplicantExamSubject() => 
            ((double) this.GetAllApplicantExamSubject().Count<ApplicantExamSubject>());

       
    }
}

