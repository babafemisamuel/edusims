﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface IApplicationMessageService
    {
        void AddApplicationMessage(ApplicationMessage ApplicationMessage);
        void AddOrEditApplicationMessage(ApplicationMessage ApplicationMessage);
        void EditApplicationMessage(ApplicationMessage ApplicationMessage);
        IEnumerable<ApplicationMessage> GetAllApplicationMessage();
        IEnumerable<ApplicationMessage> GetAllApplicationMessageIncludingDeleted();
        ApplicationMessage GetApplicationMessage(Guid id);
        ApplicationMessage GetByKey(Guid key);
        void RemoveApplicationMessage(Guid id);
        double TotalApplicationMessage();
    }
}

