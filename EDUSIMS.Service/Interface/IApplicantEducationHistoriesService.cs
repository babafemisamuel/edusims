﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface IApplicantEducationHistoriesService
    {
        void AddApplicantEducationHistory(ApplicantEducationHistory ApplicantEducationHistory);
        void AddOrEditApplicantEducationHistory(ApplicantEducationHistory ApplicantEducationHistory);
        void EditApplicantEducationHistory(ApplicantEducationHistory ApplicantEducationHistory);
        IEnumerable<ApplicantEducationHistory> GetAllApplicantEducationHistory();
        IEnumerable<ApplicantEducationHistory> GetAllApplicantEducationHistoryIncludingDeleted();
        ApplicantEducationHistory GetApplicantEducationHistory(Guid id);
        ApplicantEducationHistory GetByKey(Guid key);
        void RemoveApplicantEducationHistory(Guid id);
        double TotalApplicantEducationHistory();
    }
}

