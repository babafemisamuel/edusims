﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface IApplicantDirectEntryResultService
    {
        void AddApplicantDirectEntryResult(ApplicantDirectEntryResult ApplicantDirectEntryResult);
        void AddOrEditApplicantDirectEntryResult(ApplicantDirectEntryResult ApplicantDirectEntryResult);
        void EditApplicantDirectEntryResult(ApplicantDirectEntryResult ApplicantDirectEntryResult);
        IEnumerable<ApplicantDirectEntryResult> GetAllApplicantDirectEntryResult();
        IEnumerable<ApplicantDirectEntryResult> GetAllApplicantDirectEntryResultIncludingDeleted();
        ApplicantDirectEntryResult GetApplicantDirectEntryResult(Guid id);
        ApplicantDirectEntryResult GetByKey(Guid key);
        void RemoveApplicantDirectEntryResult(Guid id);
        double TotalApplicantDirectEntryResult();
    }
}

