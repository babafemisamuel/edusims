﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface ICourseLecturerService
    {
        void AddCourseLecturer(CourseLecturer CourseLecturer);
        void AddOrEditCourseLecturer(CourseLecturer CourseLecturer);
        void EditCourseLecturer(CourseLecturer CourseLecturer);
        IEnumerable<CourseLecturer> GetAllCourseLecturer();
        IEnumerable<CourseLecturer> GetAllCourseLecturerIncludingDeleted();
        CourseLecturer GetByKey(Guid key);
        CourseLecturer GetCourseLecturer(Guid id);
        void RemoveCourseLecturer(Guid id);
        double TotalCourseLecturer();
    }
}

