﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface IApplicantDegreeQualificationsService
    {
        void AddApplicantDegreeQualification(ApplicantDegreeQualification ApplicantDegreeQualification);
        void AddOrEditApplicantDegreeQualification(ApplicantDegreeQualification ApplicantDegreeQualification);
        void EditApplicantDegreeQualification(ApplicantDegreeQualification ApplicantDegreeQualification);
        IEnumerable<ApplicantDegreeQualification> GetAllApplicantDegreeQualification();
        IEnumerable<ApplicantDegreeQualification> GetAllApplicantDegreeQualificationIncludingDeleted();
        ApplicantDegreeQualification GetApplicantDegreeQualification(Guid id);
        ApplicantDegreeQualification GetByKey(Guid key);
        void RemoveApplicantDegreeQualification(Guid id);
        double TotalApplicantDegreeQualification();
    }
}

