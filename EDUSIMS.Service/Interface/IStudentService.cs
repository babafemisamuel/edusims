﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface IStudentService
    {
        void AddOrEditStudent(Student Student);
        void AddStudent(Student Student);
        void EditStudent(Student Student);
        IEnumerable<Student> GetAllStudent();
        IEnumerable<Student> GetAllStudentIncludingDeleted();
        Student GetByKey(Guid key);
        Student GetStudent(Guid id);
        void RemoveStudent(Guid id);
        double TotalStudent();
    }
}

