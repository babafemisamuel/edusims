﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface IApplicantExamResultService
    {
        void AddApplicantExamResult(ApplicantExamResult ApplicantExamResult);
        void AddOrEditApplicantExamResult(ApplicantExamResult ApplicantExamResult);
        void EditApplicantExamResult(ApplicantExamResult ApplicantExamResult);
        IEnumerable<ApplicantExamResult> GetAllApplicantExamResult();
        IEnumerable<ApplicantExamResult> GetAllApplicantExamResultIncludingDeleted();
        ApplicantExamResult GetApplicantExamResult(Guid id);
        ApplicantExamResult GetByKey(Guid key);
        void RemoveApplicantExamResult(Guid id);
        double TotalApplicantExamResult();
    }
}

