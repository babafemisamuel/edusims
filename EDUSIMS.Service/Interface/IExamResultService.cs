﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface IExamResultService
    {
        void AddExamResult(ExamResult ExamResult);
        void AddOrEditExamResult(ExamResult ExamResult);
        void EditExamResult(ExamResult ExamResult);
        IEnumerable<ExamResult> GetAllExamResult();
        IEnumerable<ExamResult> GetAllExamResultIncludingDeleted();
        ExamResult GetByKey(Guid key);
        ExamResult GetExamResult(Guid id);
        void RemoveExamResult(Guid id);
        double TotalExamResult();
    }
}

