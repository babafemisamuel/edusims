﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface IAppUserService
    {
        void AddAppUser(AppUser AppUser);
        void AddOrEditAppUser(AppUser AppUser);
        void EditAppUser(AppUser AppUser);
        IEnumerable<AppUser> GetAllAppUser();
        IEnumerable<AppUser> GetAllAppUserIncludingDeleted();
        AppUser GetAppUser(Guid id);
        AppUser GetByKey(Guid key);
        void RemoveAppUser(Guid id);
        double TotalAppUser();
    }
}

