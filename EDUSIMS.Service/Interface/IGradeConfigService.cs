﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface IGradeConfigService
    {
        void AddGradeConfig(GradeConfig GradeConfig);
        void AddOrEditGradeConfig(GradeConfig GradeConfig);
        void EditGradeConfig(GradeConfig GradeConfig);
        IEnumerable<GradeConfig> GetAllGradeConfig();
        IEnumerable<GradeConfig> GetAllGradeConfigIncludingDeleted();
        GradeConfig GetByKey(Guid key);
        GradeConfig GetGradeConfig(Guid id);
        void RemoveGradeConfig(Guid id);
        double TotalGradeConfig();
    }
}

