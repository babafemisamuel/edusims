﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface ICourseService
    {
        void AddCourse(Course Course);
        void AddOrEditCourse(Course Course);
        void EditCourse(Course Course);
        IEnumerable<Course> GetAllCourse();
        IEnumerable<Course> GetAllCourseIncludingDeleted();
        Course GetByKey(Guid key);
        Course GetCourse(Guid id);
        void RemoveCourse(Guid id);
        double TotalCourse();
    }
}

