﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface IStudentCourseService
    {
        void AddOrEditStudentCourse(StudentCourse StudentCourse);
        void AddStudentCourse(StudentCourse StudentCourse);
        void EditStudentCourse(StudentCourse StudentCourse);
        IEnumerable<StudentCourse> GetAllStudentCourse();
        IEnumerable<StudentCourse> GetAllStudentCourseIncludingDeleted();
        StudentCourse GetByKey(Guid key);
        StudentCourse GetStudentCourse(Guid id);
        void RemoveStudentCourse(Guid id);
        double TotalStudentCourse();
    }
}

