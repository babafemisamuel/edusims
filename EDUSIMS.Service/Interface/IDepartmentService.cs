﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface IDepartmentService
    {
        void AddDepartment(Department Department);
        void AddOrEditDepartment(Department Department);
        void EditDepartment(Department Department);
        IEnumerable<Department> GetAllDepartment();
        IEnumerable<Department> GetAllDepartmentIncludingDeleted();
        Department GetByKey(Guid key);
        Department GetDepartment(Guid id);
        void RemoveDepartment(Guid id);
        double TotalDepartment();
    }
}

