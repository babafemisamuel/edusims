﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface IApplicantPaymentService
    {
        void AddApplicantPayment(ApplicantPayment ApplicantPayment);
        void AddOrEditApplicantPayment(ApplicantPayment ApplicantPayment);
        void EditApplicantPayment(ApplicantPayment ApplicantPayment);
        IEnumerable<ApplicantPayment> GetAllApplicantPayment();
        IEnumerable<ApplicantPayment> GetAllApplicantPaymentIncludingDeleted();
        ApplicantPayment GetApplicantPayment(Guid id);
        ApplicantPayment GetByKey(Guid key);
        void RemoveApplicantPayment(Guid id);
        double TotalApplicantPayment();
    }
}

