﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface ILecturerRoleService
    {
        void AddLecturerRole(LecturerRole LecturerRole);
        void AddOrEditLecturerRole(LecturerRole LecturerRole);
        void EditLecturerRole(LecturerRole LecturerRole);
        IEnumerable<LecturerRole> GetAllLecturerRole();
        IEnumerable<LecturerRole> GetAllLecturerRoleIncludingDeleted();
        LecturerRole GetByKey(Guid key);
        LecturerRole GetLecturerRole(Guid id);
        void RemoveLecturerRole(Guid id);
        double TotalLecturerRole();
    }
}

