﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface IProgrammeService
    {
        void AddOrEditProgramme(Programme Programme);
        void AddProgramme(Programme Programme);
        void EditProgramme(Programme Programme);
        IEnumerable<Programme> GetAllProgramme();
        IEnumerable<Programme> GetAllProgrammeIncludingDeleted();
        Programme GetByKey(Guid key);
        Programme GetProgramme(Guid id);
        void RemoveProgramme(Guid id);
        double TotalProgramme();
    }
}

