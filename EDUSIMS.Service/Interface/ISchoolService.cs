﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface ISchoolService
    {
        void AddOrEditSchool(School School);
        void AddSchool(School School);
        void EditSchool(School School);
        IEnumerable<School> GetAllSchool();
        IEnumerable<School> GetAllSchoolIncludingDeleted();
        School GetByKey(Guid key);
        School GetSchool(Guid id);
        void RemoveSchool(Guid id);
        double TotalSchool();
    }
}

