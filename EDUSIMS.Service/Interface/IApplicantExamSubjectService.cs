﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface IApplicantExamSubjectService
    {
        void AddApplicantExamSubject(ApplicantExamSubject ApplicantExamSubject);
        void AddOrEditApplicantExamSubject(ApplicantExamSubject ApplicantExamSubject);
        void EditApplicantExamSubject(ApplicantExamSubject ApplicantExamSubject);
        IEnumerable<ApplicantExamSubject> GetAllApplicantExamSubject();
        IEnumerable<ApplicantExamSubject> GetAllApplicantExamSubjectIncludingDeleted();
        ApplicantExamSubject GetApplicantExamSubject(Guid id);
        ApplicantExamSubject GetByKey(Guid key);
        void RemoveApplicantExamSubject(Guid id);
        double TotalApplicantExamSubject();
    }
}

