﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface IStudentApplicationFeeService
    {
        void AddOrEditStudentApplicationFee(StudentApplicationFee StudentApplicationFee);
        void AddStudentApplicationFee(StudentApplicationFee StudentApplicationFee);
        void EditStudentApplicationFee(StudentApplicationFee StudentApplicationFee);
        IEnumerable<StudentApplicationFee> GetAllStudentApplicationFee();
        IEnumerable<StudentApplicationFee> GetAllStudentApplicationFeeIncludingDeleted();
        StudentApplicationFee GetByKey(Guid key);
        StudentApplicationFee GetStudentApplicationFee(Guid id);
        void RemoveStudentApplicationFee(Guid id);
        double TotalStudentApplicationFee();
    }
}

