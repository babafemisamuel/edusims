﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface ILecturerService
    {
        void AddLecturer(Lecturer Lecturer);
        void AddOrEditLecturer(Lecturer Lecturer);
        void EditLecturer(Lecturer Lecturer);
        IEnumerable<Lecturer> GetAllLecturer();
        IEnumerable<Lecturer> GetAllLecturerIncludingDeleted();
        Lecturer GetByKey(Guid key);
        Lecturer GetLecturer(Guid id);
        void RemoveLecturer(Guid id);
        double TotalLecturer();
    }
}

