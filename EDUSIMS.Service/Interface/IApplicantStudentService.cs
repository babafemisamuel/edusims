﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface IApplicantStudentService
    {
        void AddApplicantStudent(ApplicantStudent ApplicantStudent);
        void AddOrEditApplicantStudent(ApplicantStudent ApplicantStudent);
        void EditApplicantStudent(ApplicantStudent ApplicantStudent);
        IEnumerable<ApplicantStudent> GetAllApplicantStudent();
        IEnumerable<ApplicantStudent> GetAllApplicantStudentIncludingDeleted();
        ApplicantStudent GetApplicantStudent(Guid id);
        ApplicantStudent GetByKey(Guid key);
        void RemoveApplicantStudent(Guid id);
        double TotalApplicantStudent();
    }
}

