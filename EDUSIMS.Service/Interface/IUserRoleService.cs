﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface IUserRoleService
    {
        void AddOrEditUserRole(UserRole UserRole);
        void AddUserRole(UserRole UserRole);
        void EditUserRole(UserRole UserRole);
        IEnumerable<UserRole> GetAllUserRole();
        IEnumerable<UserRole> GetAllUserRoleIncludingDeleted();
        UserRole GetByKey(Guid key);
        UserRole GetUserRole(Guid id);
        void RemoveUserRole(Guid id);
        double TotalUserRole();
    }
}

