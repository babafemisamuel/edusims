﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface IApplicantAddressService
    {
        void AddApplicantAddress(ApplicantAddress ApplicantAddress);
        void AddOrEditApplicantAddress(ApplicantAddress ApplicantAddress);
        void EditApplicantAddress(ApplicantAddress ApplicantAddress);
        IEnumerable<ApplicantAddress> GetAllApplicantAddress();
        IEnumerable<ApplicantAddress> GetAllApplicantAddressIncludingDeleted();
        ApplicantAddress GetApplicantAddress(Guid id);
        ApplicantAddress GetByKey(Guid key);
        void RemoveApplicantAddress(Guid id);
        double TotalApplicantAddress();
    }
}

