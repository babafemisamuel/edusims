﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface IApplicantJambResultService
    {
        void AddApplicantJambResult(ApplicantJambResult ApplicantJambResult);
        void AddOrEditApplicantJambResult(ApplicantJambResult ApplicantJambResult);
        void EditApplicantJambResult(ApplicantJambResult ApplicantJambResult);
        IEnumerable<ApplicantJambResult> GetAllApplicantJambResult();
        IEnumerable<ApplicantJambResult> GetAllApplicantJambResultIncludingDeleted();
        ApplicantJambResult GetApplicantJambResult(Guid id);
        ApplicantJambResult GetByKey(Guid key);
        void RemoveApplicantJambResult(Guid id);
        double TotalApplicantJambResult();
    }
}

