﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface IBankAccountService
    {
        void AddBankAccount(BankAccount BankAccount);
        void AddOrEditBankAccount(BankAccount BankAccount);
        void EditBankAccount(BankAccount BankAccount);
        IEnumerable<BankAccount> GetAllBankAccount();
        IEnumerable<BankAccount> GetAllBankAccountIncludingDeleted();
        BankAccount GetBankAccount(Guid id);
        BankAccount GetByKey(Guid key);
        void RemoveBankAccount(Guid id);
        double TotalBankAccount();
    }
}

