﻿namespace EDUSIMS.Service.Interface
{
    using EDUSIMS.Core.Model;
    using System;
    using System.Collections.Generic;

    public interface IPaymentInstructionService
    {
        void AddOrEditPaymentInstruction(PaymentInstruction PaymentInstruction);
        void AddPaymentInstruction(PaymentInstruction PaymentInstruction);
        void EditPaymentInstruction(PaymentInstruction PaymentInstruction);
        IEnumerable<PaymentInstruction> GetAllPaymentInstruction();
        IEnumerable<PaymentInstruction> GetAllPaymentInstructionIncludingDeleted();
        PaymentInstruction GetByKey(Guid key);
        PaymentInstruction GetPaymentInstruction(Guid id);
        void RemovePaymentInstruction(Guid id);
        double TotalPaymentInstruction();
    }
}

