﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EDUSIMS.Presentation.Startup))]
namespace EDUSIMS.Presentation
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
