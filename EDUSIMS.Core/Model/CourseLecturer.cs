namespace EDUSIMS.Core.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CourseLecturer
    {
        public Guid ID { get; set; }

        public Guid SchoolID { get; set; }

        public bool Deleted { get; set; }

        public Guid LecturerID { get; set; }

        public Guid CourseID { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateDeleted { get; set; }

        public bool IsDelete { get; set; }

        public Guid Key { get; set; }

        public Guid? CreatedBy { get; set; }

        public virtual Course Cours { get; set; }

        public virtual Lecturer Lecturer { get; set; }

        public virtual School School { get; set; }
    }
}
