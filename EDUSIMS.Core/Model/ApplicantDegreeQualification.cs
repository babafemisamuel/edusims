namespace EDUSIMS.Core.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ApplicantDegreeQualification
    {
        public Guid ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(100)]
        public string Type { get; set; }

        [Required]
        [StringLength(100)]
        public string InstitutionName { get; set; }

        [Required]
        [StringLength(150)]
        public string InstitutionAddress { get; set; }

        [Required]
        [StringLength(50)]
        public string YearEarned { get; set; }

        [Required]
        [StringLength(50)]
        public string Grade { get; set; }

        public long ApplicantStudent { get; set; }

        [Required]
        [StringLength(50)]
        public string Cgpa { get; set; }

        [StringLength(300)]
        public string FileUploadPath { get; set; }

        public Guid SchoolID { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateDeleted { get; set; }

        public bool IsDelete { get; set; }

        public Guid Key { get; set; }

        public Guid? CreatedBy { get; set; }

        public virtual School School { get; set; }
    }
}
