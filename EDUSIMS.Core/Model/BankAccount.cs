namespace EDUSIMS.Core.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class BankAccount
    {
        public Guid ID { get; set; }

        public bool Deleted { get; set; }

        public Guid SchoolID { get; set; }

        [Required]
        [StringLength(150)]
        public string Bank { get; set; }

        public int AccountNumber { get; set; }

        [Required]
        [StringLength(250)]
        public string AccountName { get; set; }

        [Column(TypeName = "text")]
        public string Description { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateDeleted { get; set; }

        public bool IsDelete { get; set; }

        public Guid Key { get; set; }

        public Guid? CreatedBy { get; set; }

        public virtual School School { get; set; }
    }
}
