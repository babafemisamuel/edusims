namespace EDUSIMS.Core.Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class SimsModel : DbContext
    {
        public SimsModel()
            : base("name=SimsModel")
        {
        }

        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<AcademicSession> AcademicSessions { get; set; }
        public virtual DbSet<ApplicantAddress> ApplicantAddresses { get; set; }
        public virtual DbSet<ApplicantDegreeQualification> ApplicantDegreeQualifications { get; set; }
        public virtual DbSet<ApplicantDirectEntryResult> ApplicantDirectEntryResults { get; set; }
        public virtual DbSet<ApplicantEducationHistory> ApplicantEducationHistories { get; set; }
        public virtual DbSet<ApplicantExamResult> ApplicantExamResults { get; set; }
        public virtual DbSet<ApplicantExamSubject> ApplicantExamSubjects { get; set; }
        public virtual DbSet<ApplicantJambResult> ApplicantJambResults { get; set; }
        public virtual DbSet<ApplicantPayment> ApplicantPayments { get; set; }
        public virtual DbSet<ApplicantStudent> ApplicantStudents { get; set; }
        public virtual DbSet<ApplicationMessage> ApplicationMessages { get; set; }
        public virtual DbSet<AppUser> AppUsers { get; set; }
        public virtual DbSet<BankAccount> BankAccounts { get; set; }
        public virtual DbSet<CourseLecturer> CourseLecturers { get; set; }
        public virtual DbSet<Cours> Courses { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<ExamResult> ExamResults { get; set; }
        public virtual DbSet<GradeConfig> GradeConfigs { get; set; }
        public virtual DbSet<LecturerRole> LecturerRoles { get; set; }
        public virtual DbSet<Lecturer> Lecturers { get; set; }
        public virtual DbSet<PaymentInstruction> PaymentInstructions { get; set; }
        public virtual DbSet<Programme> Programmes { get; set; }
        public virtual DbSet<School> Schools { get; set; }
        public virtual DbSet<StudentApplicationFee> StudentApplicationFees { get; set; }
        public virtual DbSet<StudentCours> StudentCourses { get; set; }
        public virtual DbSet<Student> Students { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ApplicantDirectEntryResult>()
                .Property(e => e.InstitutionAddress)
                .IsUnicode(false);

            modelBuilder.Entity<ApplicantStudent>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<BankAccount>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Cours>()
                .HasMany(e => e.CourseLecturers)
                .WithRequired(e => e.Cours)
                .HasForeignKey(e => e.CourseID);

            modelBuilder.Entity<Cours>()
                .HasMany(e => e.ExamResults)
                .WithRequired(e => e.Cours)
                .HasForeignKey(e => e.CourseID);

            modelBuilder.Entity<Cours>()
                .HasMany(e => e.StudentCourses)
                .WithRequired(e => e.Cours)
                .HasForeignKey(e => e.CourseID);

            modelBuilder.Entity<Programme>()
                .HasMany(e => e.ApplicantStudents)
                .WithOptional(e => e.Programme)
                .HasForeignKey(e => e.Programme_ID);

            modelBuilder.Entity<School>()
                .HasMany(e => e.LecturerRoles)
                .WithOptional(e => e.School)
                .HasForeignKey(e => e.School_ID);
        }
    }
}
