namespace EDUSIMS.Core.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ApplicantPayment
    {
        public Guid ID { get; set; }

        public Guid SchoolID { get; set; }

        public Guid? ApplicantStudentID { get; set; }

        [Required]
        [StringLength(200)]
        public string PaymentMethod { get; set; }

        public decimal AmountPaid { get; set; }

        [StringLength(300)]
        public string Bank { get; set; }

        [Required]
        [StringLength(200)]
        public string PaymentReferenceNumber { get; set; }

        [Required]
        [StringLength(200)]
        public string rrr { get; set; }

        [StringLength(50)]
        public string PaymentVerificationStatus { get; set; }

        public long? PaymentVerifiedBy { get; set; }

        public DateTime? DatePaymentVerified { get; set; }

        public int PaymentType { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateDeleted { get; set; }

        public bool IsDelete { get; set; }

        public Guid Key { get; set; }

        public Guid? CreatedBy { get; set; }

        public virtual ApplicantStudent ApplicantStudent { get; set; }
    }
}
