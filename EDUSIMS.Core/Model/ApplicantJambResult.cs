namespace EDUSIMS.Core.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ApplicantJambResult
    {
        public Guid ID { get; set; }

        public Guid SchoolID { get; set; }

        public Guid ApplicantStudentID { get; set; }

        public int JambYear { get; set; }

        [Required]
        [StringLength(100)]
        public string Subject1 { get; set; }

        [Required]
        [StringLength(100)]
        public string Subject2 { get; set; }

        [Required]
        [StringLength(100)]
        public string Subject3 { get; set; }

        [Required]
        [StringLength(100)]
        public string Subject4 { get; set; }

        public int Score1 { get; set; }

        public int Score2 { get; set; }

        public int Score3 { get; set; }

        public int Score4 { get; set; }

        [Required]
        [StringLength(300)]
        public string ExamNumber { get; set; }

        [Required]
        [StringLength(300)]
        public string RegistrationNumber { get; set; }

        [StringLength(300)]
        public string FileUploadPath { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateDeleted { get; set; }

        public bool IsDelete { get; set; }

        public Guid Key { get; set; }

        public Guid? CreatedBy { get; set; }

        public virtual ApplicantStudent ApplicantStudent { get; set; }
    }
}
