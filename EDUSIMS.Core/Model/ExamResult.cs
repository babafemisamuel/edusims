namespace EDUSIMS.Core.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ExamResult
    {
        public Guid ID { get; set; }

        public Guid SchoolID { get; set; }

        public bool Deleted { get; set; }

        public decimal CA1Score { get; set; }

        public decimal CA2Score { get; set; }

        public decimal ExamScore { get; set; }

        [Required]
        [StringLength(10)]
        public string Grade { get; set; }

        public Guid CourseID { get; set; }

        public Guid StudentID { get; set; }

        public Guid AcademicSessionID { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateDeleted { get; set; }

        public bool IsDelete { get; set; }

        public Guid Key { get; set; }

        public Guid? CreatedBy { get; set; }

        public virtual AcademicSession AcademicSession { get; set; }

        public virtual Course Cours { get; set; }

        public virtual Student Student { get; set; }
    }
}
