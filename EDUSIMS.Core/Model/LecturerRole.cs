namespace EDUSIMS.Core.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class LecturerRole
    {
        public Guid ID { get; set; }

        public bool Deleted { get; set; }

        public Guid LecturerID { get; set; }

        [Required]
        [StringLength(300)]
        public string Role { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateDeleted { get; set; }

        public bool IsDelete { get; set; }

        public Guid Key { get; set; }

        public Guid? CreatedBy { get; set; }

        public Guid? School_ID { get; set; }

        public virtual School School { get; set; }
    }
}
