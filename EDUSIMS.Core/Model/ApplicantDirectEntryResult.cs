namespace EDUSIMS.Core.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ApplicantDirectEntryResult
    {
        public Guid ID { get; set; }

        public Guid SchoolID { get; set; }

        public Guid ApplicantStudentID { get; set; }

        [Required]
        [StringLength(500)]
        public string Institution { get; set; }

        [Required]
        [StringLength(100)]
        public string ALevel { get; set; }

        public int GraduationYear { get; set; }

        [Column(TypeName = "text")]
        public string InstitutionAddress { get; set; }

        [StringLength(300)]
        public string FileUploadPath { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateDeleted { get; set; }

        public bool IsDelete { get; set; }

        public Guid Key { get; set; }

        public Guid? CreatedBy { get; set; }

        public virtual ApplicantStudent ApplicantStudent { get; set; }

        public virtual School School { get; set; }
    }
}
