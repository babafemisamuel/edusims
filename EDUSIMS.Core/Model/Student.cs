namespace EDUSIMS.Core.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Student
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Student()
        {
            ExamResults = new HashSet<ExamResult>();
            StudentCourses = new HashSet<StudentCourse>();
        }

        public Guid ID { get; set; }

        public bool IsActive { get; set; }

        public Guid? SchoolID { get; set; }

        [Required]
        [StringLength(200)]
        public string Firstname { get; set; }

        [StringLength(200)]
        public string Middlename { get; set; }

        [Required]
        [StringLength(200)]
        public string Lastname { get; set; }

        [StringLength(300)]
        public string AddressLine1 { get; set; }

        [StringLength(300)]
        public string AddressLine2 { get; set; }

        [StringLength(100)]
        public string AddressTown { get; set; }

        [StringLength(50)]
        public string AddressState { get; set; }

        [StringLength(50)]
        public string MatricNumber { get; set; }

        [StringLength(50)]
        public string CurrentClass { get; set; }

        [Required]
        [StringLength(10)]
        public string Sex { get; set; }

        public Guid? ApplicationUser { get; set; }

        public bool Deleted { get; set; }

        [StringLength(50)]
        public string Phone { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        public Guid? ProgrammeID { get; set; }

        public Guid DepartmentID { get; set; }

        [StringLength(100)]
        public string ProfileImageFileName { get; set; }

        public Guid? AcademicSessionID { get; set; }

        [StringLength(200)]
        public string ApplicationNumber { get; set; }

        [StringLength(50)]
        public string DepartmentForUpload { get; set; }

        [StringLength(50)]
        public string ProgrammeForUPload { get; set; }

        [StringLength(200)]
        public string RegistrationNumber { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateDeleted { get; set; }

        public bool IsDelete { get; set; }

        public Guid Key { get; set; }

        public Guid? CreatedBy { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ExamResult> ExamResults { get; set; }

        public virtual School School { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StudentCourse> StudentCourses { get; set; }
    }
}
