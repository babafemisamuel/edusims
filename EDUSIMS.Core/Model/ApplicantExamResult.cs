namespace EDUSIMS.Core.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ApplicantExamResult
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ApplicantExamResult()
        {
            ApplicantExamSubjects = new HashSet<ApplicantExamSubject>();
        }

        public Guid ID { get; set; }

        public Guid ApplicantStudentID { get; set; }

        [Required]
        [StringLength(300)]
        public string ExamName { get; set; }

        public int ExamYear { get; set; }

        [StringLength(200)]
        public string ExamNumber { get; set; }

        [StringLength(200)]
        public string UploadedFileName { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateDeleted { get; set; }

        public bool IsDelete { get; set; }

        public Guid Key { get; set; }

        public Guid? CreatedBy { get; set; }

        public virtual ApplicantStudent ApplicantStudent { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ApplicantExamSubject> ApplicantExamSubjects { get; set; }
    }
}
