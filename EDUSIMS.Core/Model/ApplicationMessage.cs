namespace EDUSIMS.Core.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ApplicationMessage
    {
        public Guid ID { get; set; }

        public Guid SchoolID { get; set; }

        [Required]
        [StringLength(100)]
        public string RecipientEmail { get; set; }

        [Required]
        [StringLength(500)]
        public string MessageBody { get; set; }

        [StringLength(100)]
        public string Subject { get; set; }

        [Required]
        [StringLength(200)]
        public string SenderName { get; set; }

        [StringLength(200)]
        public string SenderEmail { get; set; }

        [StringLength(50)]
        public string SenderPhone { get; set; }

        [StringLength(20)]
        public string SenderType { get; set; }

        [StringLength(50)]
        public string Gender { get; set; }

        [StringLength(50)]
        public string Phone { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateDeleted { get; set; }

        public bool IsDelete { get; set; }

        public Guid Key { get; set; }

        public Guid? CreatedBy { get; set; }

        public virtual School School { get; set; }
    }
}
