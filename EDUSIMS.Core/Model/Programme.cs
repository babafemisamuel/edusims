namespace EDUSIMS.Core.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Programme
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Programme()
        {
            ApplicantStudents = new HashSet<ApplicantStudent>();
        }

        public Guid ID { get; set; }

        public bool Deleted { get; set; }

        public Guid SchoolID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        public Guid FacultyID { get; set; }

        public Guid DepartmentID { get; set; }

        [Required]
        [StringLength(50)]
        public string Type { get; set; }

        [Required]
        [StringLength(100)]
        public string Acronym { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateDeleted { get; set; }

        public bool IsDelete { get; set; }

        public Guid Key { get; set; }

        public Guid? CreatedBy { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ApplicantStudent> ApplicantStudents { get; set; }

        public virtual Department Department { get; set; }

        public virtual School School { get; set; }
    }
}
