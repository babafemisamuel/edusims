namespace EDUSIMS.Core.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class School
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public School()
        {
            AcademicSessions = new HashSet<AcademicSession>();
            ApplicantDegreeQualifications = new HashSet<ApplicantDegreeQualification>();
            ApplicantDirectEntryResults = new HashSet<ApplicantDirectEntryResult>();
            ApplicationMessages = new HashSet<ApplicationMessage>();
            AppUsers = new HashSet<AppUser>();
            BankAccounts = new HashSet<BankAccount>();
            CourseLecturers = new HashSet<CourseLecturer>();
            Courses = new HashSet<Course>();
            Departments = new HashSet<Department>();
            GradeConfigs = new HashSet<GradeConfig>();
            LecturerRoles = new HashSet<LecturerRole>();
            PaymentInstructions = new HashSet<PaymentInstruction>();
            Programmes = new HashSet<Programme>();
            StudentApplicationFees = new HashSet<StudentApplicationFee>();
            StudentCourses = new HashSet<StudentCourse>();
            Students = new HashSet<Student>();
        }

        public Guid ID { get; set; }

        public bool IsActive { get; set; }

        public bool Deleted { get; set; }

        [Required]
        [StringLength(500)]
        public string Name { get; set; }

        [Required]
        [StringLength(500)]
        public string GuidKey { get; set; }

        [StringLength(100)]
        public string ContactEmail { get; set; }

        [StringLength(100)]
        public string ContactPhone { get; set; }

        [StringLength(100)]
        public string ContactPerson { get; set; }

        public int SubscriptionStudentCapacity { get; set; }

        public DateTime SubscriptionStart { get; set; }

        public DateTime SubscriptionFinish { get; set; }

        [StringLength(500)]
        public string Address { get; set; }

        [StringLength(100)]
        public string StudentRegistrationFormat { get; set; }

        public Guid? CurrentAcademicSession { get; set; }

        public bool? IsSessionOpenForRegistration { get; set; }

        public Guid? RegistrationSession { get; set; }

        [StringLength(50)]
        public string CurrentSemester { get; set; }

        public string Slogan { get; set; }

        public string OtherInformation { get; set; }

        [StringLength(100)]
        public string ApplicationUrl { get; set; }

        public int RegisteredCourseUnitMinimum { get; set; }

        public int RegisteredCourseUnitMaximum { get; set; }

        [StringLength(200)]
        public string LogoFileName { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateDeleted { get; set; }

        public bool IsDelete { get; set; }

        public Guid Key { get; set; }

        public Guid? CreatedBy { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AcademicSession> AcademicSessions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ApplicantDegreeQualification> ApplicantDegreeQualifications { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ApplicantDirectEntryResult> ApplicantDirectEntryResults { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ApplicationMessage> ApplicationMessages { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppUser> AppUsers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BankAccount> BankAccounts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CourseLecturer> CourseLecturers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Course> Courses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Department> Departments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GradeConfig> GradeConfigs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LecturerRole> LecturerRoles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PaymentInstruction> PaymentInstructions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Programme> Programmes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StudentApplicationFee> StudentApplicationFees { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StudentCourse> StudentCourses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Student> Students { get; set; }
    }
}
