namespace EDUSIMS.Core.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Lecturer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Lecturer()
        {
            CourseLecturers = new HashSet<CourseLecturer>();
        }

        public Guid ID { get; set; }

        [Required]
        [StringLength(200)]
        public string Firstname { get; set; }

        [StringLength(200)]
        public string Middlename { get; set; }

        [Required]
        [StringLength(200)]
        public string Lastname { get; set; }

        public Guid? ApplicationUser { get; set; }

        public bool Deleted { get; set; }

        [Required]
        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(50)]
        public string ProfileImageFileName { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        public Guid Faculty { get; set; }

        [StringLength(100)]
        public string Phone { get; set; }

        public string Address { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateDeleted { get; set; }

        public bool IsDelete { get; set; }

        public Guid Key { get; set; }

        public Guid? CreatedBy { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CourseLecturer> CourseLecturers { get; set; }
    }
}
