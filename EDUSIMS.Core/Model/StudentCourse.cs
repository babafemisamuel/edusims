namespace EDUSIMS.Core.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StudentCourses")]
    public partial class StudentCourse
    {
        public Guid ID { get; set; }

        public Guid SchoolID { get; set; }

        public bool Deleted { get; set; }

        public Guid CourseID { get; set; }

        public Guid StudentID { get; set; }

        [Required]
        [StringLength(100)]
        public string ClassLevel { get; set; }

        public Guid AcademicSessionID { get; set; }

        [Required]
        [StringLength(50)]
        public string Semester { get; set; }

        [StringLength(50)]
        public string ApprovalStatus { get; set; }

        public string ApprovalComments { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateDeleted { get; set; }

        public bool IsDelete { get; set; }

        public Guid Key { get; set; }

        public Guid? CreatedBy { get; set; }

        public virtual Course Cours { get; set; }

        public virtual School School { get; set; }

        public virtual Student Student { get; set; }
    }
}
