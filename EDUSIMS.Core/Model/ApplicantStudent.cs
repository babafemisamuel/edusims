namespace EDUSIMS.Core.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ApplicantStudent
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ApplicantStudent()
        {
            ApplicantAddresses = new HashSet<ApplicantAddress>();
            ApplicantDirectEntryResults = new HashSet<ApplicantDirectEntryResult>();
            ApplicantEducationHistories = new HashSet<ApplicantEducationHistory>();
            ApplicantExamResults = new HashSet<ApplicantExamResult>();
            ApplicantJambResults = new HashSet<ApplicantJambResult>();
            ApplicantPayments = new HashSet<ApplicantPayment>();
        }

        public Guid ID { get; set; }

        public Guid SchoolID { get; set; }

        [StringLength(100)]
        public string RegistrationNumber { get; set; }

        [Required]
        [StringLength(200)]
        public string Firstname { get; set; }

        [StringLength(200)]
        public string Middlename { get; set; }

        [Required]
        [StringLength(200)]
        public string Lastname { get; set; }

        [Required]
        [StringLength(10)]
        public string Sex { get; set; }

        [StringLength(50)]
        public string Phone { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        public long? ApplicationApprovalMadeBy { get; set; }

        [Required]
        [StringLength(100)]
        public string ApplicationStatus { get; set; }

        public DateTime? DateApplicationApproved { get; set; }

        public long ChoiceProgramme1 { get; set; }

        public long? ChoiceProgramme2 { get; set; }

        public long? AcademicSession { get; set; }

        [Required]
        [StringLength(50)]
        public string StudyMode { get; set; }

        [StringLength(50)]
        public string EntryMode { get; set; }

        public DateTime? DateofBirth { get; set; }

        [Column(TypeName = "text")]
        public string Address { get; set; }

        [StringLength(200)]
        public string City { get; set; }

        [StringLength(100)]
        public string StateofOrigin { get; set; }

        [StringLength(100)]
        public string Nationality { get; set; }

        [StringLength(20)]
        public string Title { get; set; }

        public int? Score { get; set; }

        public long? ApprovedProgramme { get; set; }

        public bool? IsAdmissionAccepted { get; set; }

        [StringLength(50)]
        public string ApprovedClassLevel { get; set; }

        [StringLength(500)]
        public string FolderGuid { get; set; }

        [Required]
        [StringLength(100)]
        public string ProgrammeType { get; set; }

        [StringLength(200)]
        public string ProfileImage { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateDeleted { get; set; }

        public bool IsDelete { get; set; }

        public Guid Key { get; set; }

        public Guid? CreatedBy { get; set; }

        public Guid? Programme_ID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ApplicantAddress> ApplicantAddresses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ApplicantDirectEntryResult> ApplicantDirectEntryResults { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ApplicantEducationHistory> ApplicantEducationHistories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ApplicantExamResult> ApplicantExamResults { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ApplicantJambResult> ApplicantJambResults { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ApplicantPayment> ApplicantPayments { get; set; }

        public virtual Programme Programme { get; set; }
    }
}
