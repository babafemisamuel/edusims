namespace EDUSIMS.Core.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ApplicantEducationHistory
    {
        public Guid ID { get; set; }

        [Required]
        [StringLength(100)]
        public string InstitutionName { get; set; }

        [Required]
        [StringLength(50)]
        public string EducationLevel { get; set; }

        public int YearStart { get; set; }

        public int YearEnd { get; set; }

        public Guid ApplicantStudentID { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateDeleted { get; set; }

        public bool IsDelete { get; set; }

        public Guid Key { get; set; }

        public Guid? CreatedBy { get; set; }

        public virtual ApplicantStudent ApplicantStudent { get; set; }
    }
}
