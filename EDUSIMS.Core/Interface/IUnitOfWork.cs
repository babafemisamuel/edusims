﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EDUSIMS.Core.Interface
{
    public interface IUnitOfWork
    {
        // Methods
        ISims<T> RepositoryFor<T>() where T : class;
        void Save();
    }


}
