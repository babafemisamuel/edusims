﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EDUSIMS.Core.Interface
{
    public interface ISims<TEntity> where TEntity : class
    {
        // Methods
        void Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);
        void Edit(TEntity entity);
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
        TEntity FindByID(Func<TEntity, bool> predicate);
        TEntity Get(Guid id);
        IEnumerable<TEntity> GetAll();
        TEntity Include(TEntity entity);
        IEnumerable<TEntity> IncludeRange(IEnumerable<TEntity> entities);
        void Remove(TEntity entity);
        void RemoveRange(IEnumerable<TEntity> entity);
    }
}
