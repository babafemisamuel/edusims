﻿using EDUSIMS.Core.Interface;
using EDUSIMS.Infrastructure.Model;
using EDUSIMS.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EDUSIMS.Infrastructure.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        // Fields
        private readonly SimsModel _db = new SimsModel();
        private bool disposed = false;

        // Methods
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this._db.Dispose();
                }
                this.disposed = true;
            }
        }

        public ISims<T> RepositoryFor<T>() where T : class =>
            new SimsRepository<T>(this._db);

        public void Save()
        {
            this._db.SaveChanges();
        }
    }



}