﻿using EDUSIMS.Core.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EDUSIMS.Infrastructure.Model
{
    public partial class SimsModel : DbContext
    {
        public SimsModel()
            : base("name=SimsModel")
        {
        }

       
        public virtual DbSet<AcademicSession> AcademicSessions { get; set; }
        public virtual DbSet<ApplicantAddress> ApplicantAddresses { get; set; }
        public virtual DbSet<ApplicantDegreeQualification> ApplicantDegreeQualifications { get; set; }
        public virtual DbSet<ApplicantDirectEntryResult> ApplicantDirectEntryResults { get; set; }
        public virtual DbSet<ApplicantEducationHistory> ApplicantEducationHistories { get; set; }
        public virtual DbSet<ApplicantExamResult> ApplicantExamResults { get; set; }
        public virtual DbSet<ApplicantExamSubject> ApplicantExamSubjects { get; set; }
        public virtual DbSet<ApplicantJambResult> ApplicantJambResults { get; set; }
        public virtual DbSet<ApplicantPayment> ApplicantPayments { get; set; }
        public virtual DbSet<ApplicantStudent> ApplicantStudents { get; set; }
        public virtual DbSet<ApplicationMessage> ApplicationMessages { get; set; }
        public virtual DbSet<AppUser> AppUsers { get; set; }
        public virtual DbSet<BankAccount> BankAccounts { get; set; }
        public virtual DbSet<CourseLecturer> CourseLecturers { get; set; }
        public virtual DbSet<Course> Courses { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<ExamResult> ExamResults { get; set; }
        public virtual DbSet<GradeConfig> GradeConfigs { get; set; }
        public virtual DbSet<LecturerRole> LecturerRoles { get; set; }
        public virtual DbSet<Lecturer> Lecturers { get; set; }
        public virtual DbSet<PaymentInstruction> PaymentInstructions { get; set; }
        public virtual DbSet<Programme> Programmes { get; set; }
        public virtual DbSet<School> Schools { get; set; }
        public virtual DbSet<StudentApplicationFee> StudentApplicationFees { get; set; }
        public virtual DbSet<StudentCourse> StudentCourses { get; set; }
        public virtual DbSet<Student> Students { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }

     
    }
}