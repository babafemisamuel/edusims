﻿using EDUSIMS.Core.Interface;
using EDUSIMS.Infrastructure.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace EDUSIMS.Infrastructure.Repository
{
    public class SimsRepository<TEntity> : ISims<TEntity> where TEntity : class
    {
        // Fields
        protected readonly SimsModel db;

        // Methods
        public SimsRepository(SimsModel db)
        {
            this.db = db;
        }

        public void Add(TEntity entity)
        {
            this.db.Set<TEntity>().Add(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            this.db.Set<TEntity>().AddRange(entities);
        }

        public void Edit(TEntity entity)
        {
            this.db.Entry<TEntity>(entity).State = EntityState.Modified;
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate) =>
            this.db.Set<TEntity>().Where<TEntity>(predicate);

        public TEntity FindByID(Func<TEntity, bool> predicate) =>
            this.db.Set<TEntity>().FirstOrDefault<TEntity>(predicate);

        public TEntity Get(Guid id)
        {
            object[] keyValues = new object[] { id };
            return this.db.Set<TEntity>().Find(keyValues);
        }

        public IEnumerable<TEntity> GetAll() =>
            this.db.Set<TEntity>().ToList<TEntity>();

        public TEntity Include(TEntity entity) =>
            default(TEntity);

        public IEnumerable<TEntity> IncludeRange(IEnumerable<TEntity> entities)
        {
            throw new NotImplementedException();
        }

        public void Remove(TEntity entity)
        {
            this.db.Set<TEntity>().Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entity)
        {
            this.db.Set<TEntity>().RemoveRange(entity);
        }
    }


}
